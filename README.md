# hse-ui


## :memo: Description 

Useful helper VIs and tools for creating and manipulating user interfaces


## :camera: Screenshot 

tbd.


## :rocket: Installation

The latest release version can be found at 
https://dokuwiki.hampel-soft.com/code/hse-libraries/hse-ui

These libraries depend on the hse-logger:
https://dokuwiki.hampel-soft.com/code/hse-libraries/hse-ui


## :bulb: Usage

tbd.


## :wrench: Configuration 

tbd. or n/a?


## :busts_in_silhouette: Contributing 

Please contribute! We'd love to see code coming back to this repo. Get in touch if you want to know more about contributing.

##  :beers: Credits

* Joerg Hampel
* Manuel Sebald

## :page_facing_up: License 

This project is licensed under a modified BSD License - see the [LICENSE](LICENSE) file for details